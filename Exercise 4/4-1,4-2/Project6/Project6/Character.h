#pragma once

class Character
{
public:
	Character();
	~Character();

	int slash();



private:

	char name;
	int HP;
	int MP;

	int str;
	int dex;
	int vit;
	int mag;
	int spr;
	int luk;

	int atk;
	int atkPer;
	int def;
	int defPer;
	int matk;
	int matkPer;
	int mdef;
	int mdefper;

};



