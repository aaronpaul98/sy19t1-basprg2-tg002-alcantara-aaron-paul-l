#include "Spell.h"



Spell::Spell()
{
	sName = "FireBall";
	sCost = 10;
	sDamage = 20;
}


Spell::Spell(std::string name, int cost, int damage)
{
	sName = name;
	sCost = cost;
	sDamage = damage;
}

Spell::~Spell()
{
}

