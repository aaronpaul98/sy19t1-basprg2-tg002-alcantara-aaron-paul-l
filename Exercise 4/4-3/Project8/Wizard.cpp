#include "Wizard.h"
#include "Spell.h"




Wizard::Wizard(std::string name, int hp, int mp)
{
	wName = name;
	wHP = hp;
	wMP = mp;
	
}

Wizard::~Wizard()
{
}



int Wizard::castSpell(Spell spell,int& mp) {
	if (wMP > spell.sCost) {
		mp -= spell.sCost;
		return spell.sDamage;
	}
	return 0;
}
