#pragma once
#include<iostream>
#include "Spell.h"

class Wizard
{
public:
	Wizard(std::string name,int hp,int mp);
	~Wizard();

	int castSpell(Spell spell, int& mp);

	

	std::string wName;
	int wHP;
	int wMP;

private:

	Spell fireball;
};

