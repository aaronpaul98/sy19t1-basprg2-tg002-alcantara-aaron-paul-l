#include<iostream>;
#include"Spell.h"
#include"Wizard.h"


using namespace std;

int main() {
	
	Wizard wizard1("Merlin",100,100);
	Wizard wizard2("Naru",100,100);
	Spell FireBall("FireBall",10,20);
	int turn=0;

	while ((wizard1.wHP > 0 && wizard1.wMP > 0) && (wizard2.wHP > 0 && wizard2.wMP > 0)) {
		if (turn == 0) {
			wizard2.wHP -= wizard1.castSpell(FireBall, wizard1.wMP);
			cout << "Merlin HP:" << wizard1.wHP << endl;
			cout << "Merlin MP:" << wizard1.wMP << endl;
			cout << "                                            " << endl;
			turn++;
		}
		else {
			wizard1.wHP -= wizard2.castSpell(FireBall, wizard2.wMP);
			cout << "Naru HP:" << wizard2.wHP << endl;
			cout << "Naru MP:" << wizard2.wMP << endl;
			cout << "----------------------------------------------------" << endl;
			turn--;
		}
	}

	if (wizard1.wHP > 0 && wizard2.wHP <= 0)
		cout << "Merlin wins" << endl;
	else
		cout << "Naru wins" << endl;
	
	system("pause");
		
}