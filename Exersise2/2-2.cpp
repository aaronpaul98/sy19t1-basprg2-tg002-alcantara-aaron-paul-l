#include <iostream>
#include <string>
#include <time.h>

using namespace std;

// Rolls dice for players/ai

void rollDice(int& dice1, int& dice2) {

	dice1 = 1 + rand() % 6;
	dice2 = 1 + rand() % 6;
}

int main() {
	srand(time(NULL));
	int playerDice1;
	int playerDice2;

	int aiDice1 = 0;
	int aiDice2 = 0;

	rollDice(aiDice1, aiDice2);
	cout << "ai rolled:" << aiDice1 << " " << aiDice2 << endl;

	rollDice(playerDice1, playerDice2);
	cout << "player rolled:" << playerDice1 << " " << playerDice2 << endl;

	system("pause");
}