#include <iostream>
#include <string>
#include <time.h>

using namespace std;

//evaluates how much the player should win/lose depending on the outcome of the dice roll

void payout(int playerDice1, int playerDice2, int aiDice1, int aiDice2, int& gold, int bet) {

	if ((playerDice1 == 1 && playerDice2 == 1) && (aiDice1 > 1 || aiDice2 > 1))
		gold += bet * 3;
	else if ((aiDice1 == 1 && aiDice2 == 1) && (playerDice1 > 1 || playerDice2 > 1))
		gold -= bet;
	else if ((playerDice1 + playerDice2) > (aiDice1 + aiDice2))
		gold += bet;
	else if ((playerDice1 + playerDice2) == (aiDice1 + aiDice2))
		gold = gold;

	else gold -= bet;

}

void rollDice(int& dice1, int& dice2) {

	dice1 = 1 + rand() % 6;
	dice2 = 1 + rand() % 6;
}

void voidBet(int& gold, int& bet) {

	cout << "Enter Bet" << endl;
	cin >> bet;
	cout << "Bet:" << bet << endl;
}

int main() {
	int gold = 1000;
	int bet;
	srand(time(NULL));
	int playerDice1;
	int playerDice2;
	int aiDice1 = 0;
	int aiDice2 = 0;


	voidBet(gold, bet);

	rollDice(aiDice1, aiDice2);
	cout << "ai rolled:" << aiDice1 << " " << aiDice2 << endl;

	rollDice(playerDice1, playerDice2);
	cout << "player rolled:" << playerDice1 << " " << playerDice2 << endl;


	payout(playerDice1, playerDice2, aiDice1, aiDice2, gold, bet);

	cout << "player money:" << gold << endl;

	system("pause");
}