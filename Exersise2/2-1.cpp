#include <iostream>
#include <string>

using namespace std;

/*
int bet(int gold) // You cannot deduct player�s gold. Function is solely used for determining bet
You cannot deduct gold here because you dont have access to the address where the gold is stored, 
what we have as a varriable is a copy of the value inside said address.

int bet(int& gold) // You can deduct player�s gold and return the bet as normal variable.
We can set the value for gold in the function because gold is referenced.
However bet is not therefore it is a varriable local to the function, this makes it so that the main cant access it directly.

void bet(int& gold, int& bet) // You can deduct player�s gold and bet will be set via referencing.
since both gold and bet is stored in main and we have them both referenced, we can forego returning a value and just perform the functions.

*/

int intBet(int gold); // get bet return int

void voidBet(int& gold); // get bet change without return

int intBet(int gold) {
	int bet;

	cout << "Enter Bet" << endl;
	cin >> bet;
	cout << "Bet:" << bet << endl;

	return gold - bet;
}

void voidBet(int& gold) {
	int bet;

	cout << "Enter Bet" << endl;
	cin >> bet;
	cout << "Bet:" << bet << endl;

}

int main() {

	int money = 1000;

	money = intBet(money);
	cout << money << endl;

	voidBet(money);
	cout << money << endl;

	system("pause");
}