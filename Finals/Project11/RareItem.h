#pragma once
#include "Item.h"
class RareItem :
	public Item
{
public:
	RareItem(string name);
	~RareItem();
	void effect(Player* target);
};

