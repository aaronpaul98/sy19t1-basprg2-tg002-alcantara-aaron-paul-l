#pragma once
#include <iostream>
#include "Player.h"

class Player;
using namespace std;

class Item
{
public:
	Item(string name);
	~Item();
	string getName();
	virtual void effect(Player* target);

protected:
	string sName;
};

