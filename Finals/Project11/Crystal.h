#pragma once
#include "Item.h"
class Crystal :
	public Item
{
public:
	Crystal();
	~Crystal();

	void effect(Player* target);
};

