#include "Bomb.h"



Bomb::Bomb()
	:Item("Bomb")
{
}


Bomb::~Bomb()
{
}

void Bomb::effect(Player * target)
{
	target->affectHealth(-25);
}
