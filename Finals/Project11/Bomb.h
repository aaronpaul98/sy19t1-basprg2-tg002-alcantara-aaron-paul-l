#pragma once
#include "Item.h"
class Bomb :
	public Item
{
public:
	Bomb();
	~Bomb();
	void effect(Player* target);
};

