#pragma once
#include <vector>
#include "Item.h"

class Item;

using namespace std;

class Player
{
public:
	Player(string name);
	~Player();

	void affectHealth(int unit);
	void gainPoints(int unit);
	void gainCrystals(int unit);
	void buyItem();
	void countItems();
	void enumerateItems();
	int getHealth();
	int getPoints();
	int getCrystals();

private:
	string sName;
	int health;
	int points;
	int crystals;
	vector<Item*> ItemList;
};

