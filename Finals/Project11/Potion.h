#pragma once
#include "Item.h"
class Potion :
	public Item
{
public:
	Potion();
	~Potion();
	void effect(Player* target);
};

