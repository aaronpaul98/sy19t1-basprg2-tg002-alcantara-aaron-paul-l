#include "Potion.h"



Potion::Potion()
	:Item("Potion")
{
}


Potion::~Potion()
{
}

void Potion::effect(Player * target)
{
	target->affectHealth(30);
}
