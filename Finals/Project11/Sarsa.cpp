#include<iostream>
#include "Player.h"

using namespace std;

int main() {
	
	Player* Hero = new Player("Hero");

	while (Hero->getCrystals() > 0 && Hero->getHealth() > 0 && Hero->getPoints() < 100)
	{
		cout << "-------------------------------" << endl;
		cout << "Rarity Points:" << Hero->getPoints() <<endl;
		cout << "Crystals:" << Hero->getCrystals() << endl;
		cout << "Health:" << Hero->getHealth() << endl;
		cout << "-------------------------------" << endl;
		Hero->buyItem();
		system("cls");
	}

	if(Hero->getCrystals() <= 0){ 
		

		cout << "  ________                        ________                     " << endl;
		cout << " /  _____/_____    _____   ____   \\_____  \\___  __ ___________ " << endl;
		cout << "/   \\  ___\\__  \\  /     \\_/ __ \\   /   |   \\  \\/ // __ \\_  __ \\" << endl;
		cout << "\\    \\_\\  \\/ __ \\|  Y Y  \\  ___/  /    |    \\   /\\  ___/|  | \\/" << endl;
		cout << " \\______  (____  /__|_|  /\\___  > \\_______  /\\_/  \\___  >__|   " << endl;
		cout << "        \\/     \\/      \\/     \\/          \\/          \\/       " << endl;
		cout << "-------------------------------" << endl;
		Hero->enumerateItems();
		cout << "-------------------------------" << endl;
		Hero->countItems();
		cout << "-------------------------------" << endl;
	}
	else if (Hero->getPoints() >= 100) {
		cout << "_____.___.               __      __.__         " << endl;
		cout << "\\__  |   | ____  __ __  /  \\    /  \\__| ____  " << endl;
		cout << " /   |   |/  _ \\|  |  \\ \\   \\/\\/   /  |/    \\ " << endl;
		cout << " \\____   (  <_> )  |  /  \\        /|  |   |  \\" << endl;
		cout << " / ______|\\____/|____/   \\ \__/\\  / |__|___|  /" << endl;
		cout << " \\/                            \\/          \\/ " << endl;
		cout << "-------------------------------" << endl;
		Hero->enumerateItems();
		cout << "-------------------------------" << endl;
		Hero->countItems();
		cout << "-------------------------------" << endl;
	}
	else if (Hero->getHealth() <= 0){
		cout << "  ________                        ________                     " << endl;
		cout << " /  _____/_____    _____   ____   \\_____  \\___  __ ___________ " << endl;
		cout << "/   \\  ___\\__  \\  /     \\_/ __ \\   /   |   \\  \\/ // __ \\_  __ \\" << endl;
		cout << "\\    \\_\\  \\/ __ \\|  Y Y  \\  ___/  /    |    \\   /\\  ___/|  | \\/" << endl;
		cout << " \\______  (____  /__|_|  /\\___  > \\_______  /\\_/  \\___  >__|   " << endl;
		cout << "        \\/     \\/      \\/     \\/          \\/          \\/       " << endl;
		cout << "-------------------------------" << endl;
		Hero->enumerateItems();
		cout << "-------------------------------" << endl;
		Hero->countItems();
		cout << "-------------------------------" << endl;

	}

	system("pause");
	return 0;
}