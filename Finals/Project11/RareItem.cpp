#include "RareItem.h"



RareItem::RareItem(string name)
	:Item(name)
{
}


RareItem::~RareItem()
{
}

void RareItem::effect(Player* target)
{
	if(sName == "SSR")
		target->gainPoints(50);
	else if (sName == "SR")
		target->gainPoints(10);
	else if	(sName == "R")
		target->gainPoints(1);
}
