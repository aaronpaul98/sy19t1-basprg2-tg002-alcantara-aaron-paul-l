#include "Crystal.h"



Crystal::Crystal()
	:Item("Crystal")
{
}


Crystal::~Crystal()
{
}

void Crystal::effect(Player* target)
{
	target->gainCrystals(15);
}
