#include "Player.h"
#include "RareItem.h"
#include <time.h>
#include "Bomb.h"
#include "Crystal.h"
#include "Potion.h"
#include <string>

using namespace std;

Player::Player(string name)
{
	sName = name;
	health = 100;
	crystals = 100;
	
}

Player::~Player()
{
}

void Player::affectHealth(int unit)
{
	health += unit;
}

void Player::gainPoints(int unit)
{
	points += unit;
}

void Player::gainCrystals(int unit)
{
	crystals += unit;
}



void Player::buyItem()
{


	/*
	1 = SSR 1%
	2 - 10 = SR 9%
	11 - 25 = Crystal 15%
	26 - 40 = Health Potion 15%
	41 - 60 = Bomb 20%
	61 - 100 = R 40%
	*/


	int output = rand() % 100+1;


	cout << output << endl;

	if (output == 1) {
	RareItem* m = new RareItem("SSR");
	m->effect(this);
	ItemList.push_back(m);
	cout << "You got a SSR wow! You gain 50 points" << endl;
	}
	else if (output >= 2 && output <= 10) {
		RareItem* m = new RareItem("SR");
		m->effect(this);
		ItemList.push_back(m);
		cout << "You got a SR nice! You gain 10 points" << endl;
	}
	else if (output >= 11 && output <= 25) {
		Crystal* m = new Crystal();
		m->effect(this);
		ItemList.push_back(m);
		cout << "You got Crystals! You gain 15 crystals" << endl;
	}
	else if (output >= 26 && output <= 40) {
	
		Potion* m = new Potion();
		m->effect(this);
		ItemList.push_back(m);
		cout << "You got a Potion! You gain 30 health" << endl;
	}
	else if (output >= 41 && output <= 60) {
		Bomb* m = new Bomb();
		m->effect(this);
		ItemList.push_back(m);
		cout << "Oh no! You got a Bomb! You lose 25 health!" << endl;
	}
	else if (output >= 61 && output <= 100) {
		RareItem* m = new RareItem("R");
		m->effect(this);
		ItemList.push_back(m);
		cout << "You got an R. You gain 1 point" << endl;
	}

	crystals -= 5;

	system("pause");
}

void Player::countItems()
{

	int ssr = 0, sr = 0, r = 0, bomb = 0, potion = 0, crystal = 0;

	for (int i = 0; i < ItemList.size(); i++) {
		if (ItemList[i]->getName() == "SSR") 
			ssr++;
		else if (ItemList[i]->getName() == "SR")
			sr++;		 
		else if (ItemList[i]->getName() == "R")
			r++;		
		else if (ItemList[i]->getName() == "Bomb")
			bomb++;	
		else if (ItemList[i]->getName() == "Potion")
			potion++;	
		else if (ItemList[i]->getName() == "Crystal")
			crystal++;	
	}

	cout << "You got:" << endl;
	cout << "SSR x " << ssr << endl;
	cout << "SR x " << sr << endl;
	cout << "R x " << r << endl;
	cout << "Bomb x " << bomb << endl;
	cout << "Potion x " << potion << endl;
	cout << "Crystal x " << crystal << endl;
}

void Player::enumerateItems()
{
	
	for (int i = 0; i < ItemList.size();i++) {
		cout << ItemList[i]->getName() << endl;
	}
}

int Player::getHealth()
{
	return health;
}

int Player::getPoints()
{
	return points;
}

int Player::getCrystals()
{
	return crystals;
}
