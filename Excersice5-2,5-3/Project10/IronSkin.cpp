#include "IronSkin.h"
#include <iostream>


IronSkin::IronSkin()
	:Skill("Iron Skin")
{
}


IronSkin::~IronSkin()
{
}

void IronSkin::activate(Unit target)
{
	cout << "You cast Iron Skin! Gain VIT+2" << endl;
	target.gainVIT(2);
}
