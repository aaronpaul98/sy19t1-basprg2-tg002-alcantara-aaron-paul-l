#include "Concentration.h"
#include <iostream>


Concentration::Concentration() 
	:Skill("Concentration")
{
}


Concentration::~Concentration()
{
}

void Concentration::activate(Unit target)
{
	cout << "You cast Concentration! Gain DEX+2" << endl;
	target.gainDEX(2);
}
