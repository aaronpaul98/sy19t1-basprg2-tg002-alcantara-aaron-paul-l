#include "Unit.h"



Unit::Unit(string name)
{
	sName = name;
	skills[0] = new Heal();
	skills[1] = new Haste();
	skills[2] = new Might();
	skills[3] = new IronSkin();
	skills[4] = new Concentration();
}

Unit::~Unit()
{
}

void Unit::gainHP(int num)
{
	HP += num;
}

void Unit::gainPOW(int num)
{
	POW += num;
}

void Unit::gainVIT(int num)
{
	VIT += num;
}

void Unit::gainAGI(int num)
{
	AGI += num;
}

void Unit::gainDEX(int num)
{
	DEX += num;
}

Skill * Unit::acessSkills(int n)
{
	return skills[n];
}
