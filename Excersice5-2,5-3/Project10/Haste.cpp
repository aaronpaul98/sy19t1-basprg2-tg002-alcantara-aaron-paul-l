#include "Haste.h"
#include <iostream>


Haste::Haste()
	:Skill("Haste")
{
}


Haste::~Haste()
{
}

void Haste::activate(Unit target)
{
	cout << "You cast Haste! Gain AGI+2" << endl;
	target.gainAGI(2);
}
