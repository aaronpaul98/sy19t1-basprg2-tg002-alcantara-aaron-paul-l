#pragma once
#include"Heal.h"
#include"Haste.h"
#include"IronSkin.h"
#include"Might.h"
#include"Concentration.h"
#include <vector>

class Unit
{
public:
	Unit(string name);
	~Unit();
	void gainHP(int num);
	void gainPOW(int num);
	void gainVIT(int num);
	void gainAGI(int num);
	void gainDEX(int num);
	Skill* acessSkills(int n);
private:
	Skill* skills[5];
	string sName;
	int HP=0;
	int POW=0;
	int AGI=0;
	int DEX=0;
	int VIT=0;
};

