#include "Might.h"
#include <iostream>


Might::Might()
	:Skill("Might")
{
}


Might::~Might()
{
}

void Might::activate(Unit target)
{
	cout << "You cast MIGHT! Gain POW+2" << endl;
	target.gainPOW(2);
}
