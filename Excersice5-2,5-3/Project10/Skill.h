#pragma once
#include "Unit.h"
#include <string>
using namespace std;
class Skill
{
public:
	Skill();
	Skill(string name);
	~Skill();
	string getName();
	virtual void activate(Unit target) = 0;
private:
	string sName;
};

