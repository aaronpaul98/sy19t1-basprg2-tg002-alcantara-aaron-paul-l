#pragma once
#include "Skill.h"
class IronSkin :
	public Skill
{
public:
	IronSkin();
	~IronSkin();
	void activate(Unit target);
};

