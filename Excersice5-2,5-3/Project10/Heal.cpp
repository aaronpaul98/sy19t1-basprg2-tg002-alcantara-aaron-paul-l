#include "Heal.h"
#include <iostream>


Heal::Heal()
	:Skill("Heal")
{
}


Heal::~Heal()
{
}

void Heal::activate(Unit target)
{
	cout << "You cast Heal! Heal 10 HP" << endl;
	target.gainHP(10);
}
