#pragma once
#include<iostream>

using namespace std;

class Character
{
public:
	Character(string t,string n, int H, int P, int V, int A, int D);
	Character();
	~Character();

	string getName();
	string getType();
	int getHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();
	int getmaxHP();

	void attack(Character* target);
	void takeDamage(int damage);
	void gainPOW(int stat);
	void gainAGI(int stat);
	void gainDEX(int stat);
	void gainVIT(int stat);
	void gainmaxHP(int stat);
	void heal();

private:
	string type;
	string name;
	int maxHP;
	int HP;
	int POW;
	int VIT;
	int AGI;
	int DEX;
};

