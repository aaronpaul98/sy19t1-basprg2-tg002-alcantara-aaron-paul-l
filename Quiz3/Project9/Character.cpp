#include "Character.h"
#include<string>





Character::Character(string t,string n, int H, int P, int V, int A, int D)
{
	type = t;
	name = n;
	maxHP = H;
	POW = P;
	VIT = V;
	AGI = A;
	DEX = D;

	HP = maxHP;

	if (t == "Wizard") {
		POW += 3;
		VIT += 1;
	}
	else if (t == "Warrior") {
		maxHP += 2;
		VIT += 2;
	}
	else if (t == "Assassin") {
		AGI += 2;
		DEX += 2;
	}
}

Character::Character()
{
}

Character::~Character()
{
}

string Character::getName()
{
	return name;
}

string Character::getType()
{
	return type;
}

int Character::getHP()
{
	return HP;
}

int Character::getPOW()
{
	return POW;
}

int Character::getVIT()
{
	return VIT;
}

int Character::getAGI()
{
	return AGI;
}

int Character::getDEX()
{
	return DEX;
}

int Character::getmaxHP()
{
	return maxHP;
}

void Character::attack(Character * target)
{
	
	float hitChance = ((float)DEX / target->getAGI()) * 100;


	if (hitChance > 80) hitChance = 80;
	else if (hitChance < 20) hitChance = 20;



	int hitRoll = rand() %100+1;
	
	
	if (hitRoll <= hitChance) {

		if ((type == "Wizard" && target->getType() == "Warrior") || (type == "Warrior" && target->getType() == "Assassin") || (type == "Assassin" && target->getType() == "Wizard")){
			
			if (((POW - target->getVIT()) * 2) > 0)
			{
				target->takeDamage((POW - target->getVIT()) * 2);
				cout << name << " deals " << ((POW - target->getVIT()) * 2) << " damage to " << target->getName() << endl;
			}
			else { target->takeDamage(1); 
			cout << name << " deals " << (1) << " damage to " << target->getName() << endl;
			}
		}
	
		else if ((type == "Wizard" && target->getType() == "Assassin") || (type == "Warrior" && target->getType() == "Wizard") || (type == "Assassin" && target->getType() == "Warrior")) {
			if (((POW - target->getVIT()) / 2) > 0) { target->takeDamage((POW - target->getVIT()) / 2); 
			cout << name << " deals " << ((POW - target->getVIT()) / 2) << " damage to " << target->getName() << endl;
			}
			else {
				target->takeDamage(1);
				cout << name << " deals " << (1) << " damage to " << target->getName() << endl;
			}
		}
		else {
			if (((POW - target->getVIT())) > 0) { target->takeDamage((POW - target->getVIT())); 
			cout << name << " deals " << ((POW - target->getVIT())) << " damage to " << target->getName() << endl;
			}
		else {
			target->takeDamage(1);
			cout << name << " deals " << (1) << " damage to " << target->getName() << endl;
		}
		}
		
		//cout << "HP" << target->getHP();
	}

	else if (hitRoll > hitChance) {
		cout << name << " missed " << endl;
	}
}

void Character::takeDamage(int damage)
{
	if (damage < HP)
		HP -= damage;
	else 
		HP = 0;
}

void Character::gainPOW(int stat)
{
	POW += stat;
}

void Character::gainAGI(int stat)
{
	AGI += stat;
}

void Character::gainDEX(int stat)
{
	DEX += stat;
}

void Character::gainVIT(int stat)
{
	VIT += stat;
}

void Character::gainmaxHP(int stat)
{
	maxHP += stat;
}

void Character::heal()
{
	if (HP + (maxHP * .3) > maxHP) HP = maxHP;
	else HP += maxHP * .3;
}
