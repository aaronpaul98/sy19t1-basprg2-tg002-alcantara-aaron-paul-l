#include"Character.h"
#include<stdio.h>
#include<string>
#include <time.h>

Character* createCharacter() {
	string job="";
	string name="";
	int choice;
	bool valid = true;

	cout << "Enter Name:";
	cin >> name;

	while (valid) {
		cout << "Choose a class [1]Assassin, [2]Wizard, [3]Warrior:";
		cin >> choice;
		switch (choice) {
		case 1:job = "Assassin";
			valid = false;
			break;
		case 2:job = "Wizard";
			valid = false;
			break;
		case 3:job = "Warrior";
			valid = false;
			break;
		default: cout << "Invalid Input"<< endl;
		}
	}
	Character* MC = new Character(job, name, 100, 10, 10, 10, 10);
	return MC;
}

Character* createMob(int level) {
	srand(time(NULL));
	string job = "";
	string name = "";
	int jobRoll = rand()%3;
	int hpRoll = rand() % (100+(level*20));
	int agiRoll = rand() % (5 +(level * 2));
	int dexRoll = rand() % (5 +(level * 2));
	int vitRoll = rand() % (5 +(level * 2));
	int powRoll = rand() % (5 +(level * 2));

	switch (jobRoll) {
	case 0:job = "Assassin";
		name = "Assassin";
		break;
	case 1:job = "Wizard";
		name = "Wizard";
		break;
	case 2:job = "Warrior";
		name = "Warrior";
		break;

	}


	Character* MC = new Character(job, name, hpRoll, vitRoll, powRoll, agiRoll, dexRoll);
	return MC;
}

void levelUp(Character* player, Character* mob) {


	cout <<"HP:" <<player->getHP() <<"/"<< player->getmaxHP() << endl;
	cout << "POW:" << player->getPOW() << endl;
	cout << "VIT:" << player->getVIT() << endl;
	cout << "AGI:" << player->getAGI() << endl;
	cout << "DEX:" << player->getDEX() << endl;
	cout << endl;
	system("pause");

	if (mob->getType() == "Wizard") {
		player->gainPOW(5);

		cout << "HP:" << player->getHP() << "/" << player->getmaxHP() << endl;
		cout << "POW:" << player->getPOW()-5 <<" +5  -> "<< player->getPOW() << endl;
		cout << "VIT:" << player->getVIT() << endl;
		cout << "AGI:" << player->getAGI() << endl;
		cout << "DEX:" << player->getDEX() << endl;

	}
	else if (mob->getType() == "Warrior") {
		player->gainmaxHP(3);
		player->gainVIT(3);

		cout << "HP:" << player->getHP() << "/" << player->getmaxHP()-3 << " +3  -> " << player->getHP() << "/" << player->getmaxHP() << endl;
		cout << "POW:" << player->getPOW() << endl;
		cout << "VIT:" << player->getVIT()-3 << " +3  -> " << player->getVIT() << endl;
		cout << "AGI:" << player->getAGI() << endl;
		cout << "DEX:" << player->getDEX() << endl;
	}
	else if (mob->getType() == "Assassin") {
		player->gainAGI(3);
		player->gainDEX(3);

		cout << "HP:" << player->getHP() << "/" << player->getmaxHP() << endl;
		cout << "POW:" << player->getPOW() << endl;
		cout << "VIT:" << player->getVIT() << endl;
		cout << "AGI:" << player->getAGI() - 3 << " +3  -> " << player->getAGI() << endl;
		cout << "DEX:" << player->getDEX() - 3 << " +3  -> " << player->getDEX() << endl;
	}
}

void combat(Character* player, Character* mob) {
	if (player->getAGI() >= mob->getAGI()) {
		while(player->getHP() != 0 && mob->getHP() != 0)
		{ 

			system("cls");

			cout << player->getName() << " HP:" << player->getHP() << endl;
			cout << "Enemy " << mob->getName() << " HP:" << mob->getHP() << endl;

			player->attack(mob);
			mob->attack(player);
			system("pause");
		}

		
	}
	else if (player->getAGI() < mob->getAGI()) {
		while (player->getHP() != 0 && mob->getHP() != 0)
		{		
			system("cls");

			cout << player->getName() << " HP:" << player->getHP() << endl;
			cout << "Enemy " << mob->getName() << " HP:" << mob->getHP() << endl;

			mob->attack(player);
			player->attack(mob);
			system("pause");
		}
	}

	if (player->getHP() == 0) {
		cout << " Y O U  D I E D" << endl;
		return;
	}
	else if (mob->getHP()==0) {
		player->heal();
		cout << player->getName() <<" has defeated Enemy " << mob->getName() << endl;
		
		levelUp(player, mob);

		delete mob;
	}
}

int main() {



	Character* MC = createCharacter();
	int level = 1;
	
	cout<<MC->getName() <<endl;
	cout << MC->getType() << endl;
	cout << MC->getHP() << endl;
	cout << MC->getPOW() << endl;
	cout << MC->getVIT() << endl;
	cout << MC->getAGI() << endl;
	cout << MC->getDEX() << endl;

	while (MC->getHP() != 0) {

		Character* enemy = createMob(level);
		cout << "new enemy has appeared " << endl;
		system("pause");


		combat(MC,enemy);
		level++;

	}

	cout << MC->getName() << "'s legacy will be remembered " << endl;
	cout << MC->getName() << " has slain " << level << "enemies" << endl;
	cout << MC->getName() << "'s final stats: " << endl;
	cout << "Class:" << MC->getType() << endl;
	cout << "HP:" << MC->getmaxHP()<< endl;
	cout << "POW:" << MC->getPOW() << endl;
	cout << "VIT:" << MC->getVIT() << endl;
	cout << "AGI:" << MC->getAGI() << endl;
	cout << "DEX:" << MC->getDEX() << endl;

	delete MC;

	system("pause");
	return 0;
}