#include <iostream>
#include <string>
#include <time.h>

using namespace std;

// 

int* makeArray() {
	int* dynInt = new int[5];
	srand(time(NULL));

	for (int i = 0; i < 5; i++) {
		*(dynInt + i) = rand() % 101;
	}

	return dynInt;
}

void deleteArray(int* dynInt) {

	delete[] dynInt;
}

int main() {

	int* dynInt = makeArray();

	for (int i = 0; i < 5; i++) {
		cout << *(dynInt + i) << endl;
	}

	deleteArray(dynInt);

	for (int i = 0; i < 5; i++) {
		cout << *(dynInt + i) << endl;
	}

	system("pause");
	return 0;
}