#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};


Item* makeItem() {
	int itemNo = rand() % 5 + 1;

	Item* item = new Item;
	srand(time(NULL));

	switch (itemNo)
	{
	case 1:	item->gold = 100;
		item->name = "Mithril Ore";
		break;
	case 2: item->gold = 50;
		item->name = "Sharp Talon";
		break;
	case 3:	item->gold = 25;
		item->name = "Thick Leather";
		break;
	case 4: item->gold = 5;
		item->name = "Jellopy";
		break;
	case 5: item->gold = 0;
		item->name = "Cursed Stone";
		break;
	}

	return item;
}


int main() {
	Item* item = makeItem();

	cout << item->gold << " " << item->name << endl;
	system("pause");
	return 0;
}