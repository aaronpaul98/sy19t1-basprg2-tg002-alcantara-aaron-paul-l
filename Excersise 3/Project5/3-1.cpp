#include <iostream>
#include <string>
#include <time.h>

using namespace std;

// GETS ARRAY AS AN INT* THEN FILLS IT WITH VALUES

void fillArray(int* randomNumbers, int size) {
	srand(time(NULL));
	for (int i = 0; i < size; i++) {
		*(randomNumbers + i) = rand() % 101;
	}
}

//prints Array
void printArray(int* randomNumbers, int size) {

	for (int i = 0; i < size; i++) {
		cout << *(randomNumbers + i) << endl;
	}

}

int main() {
	int randomNumbers[5];

	fillArray(randomNumbers, 5);
	printArray(randomNumbers, 5);

	system("pause");
	return 0;
}