#include <iostream>
#include <string>
#include <time.h>

using namespace std;
struct Item
{
	string name;
	int gold;
};


void makeItem(Item* inventory, bool& safe) {
	int itemNo = rand() % 5 + 1;

	Item* item = new Item;
	srand(time(NULL));

	switch (itemNo)
	{
	case 1:	item->gold = 100;
		item->name = "Mithril Ore";
		cout << "You found a " << item->name << " !" << endl;
		safe = true;
		break;
	case 2: item->gold = 50;
		item->name = "Sharp Talon";
		cout << "You found a " << item->name << " !" << endl;
		safe = true;
		break;
	case 3:	item->gold = 25;
		item->name = "Thick Leather";
		cout << "You found a " << item->name << " !" << endl;
		safe = true;
		break;
	case 4: item->gold = 5;
		item->name = "Jellopy";
		cout << "You found a " << item->name << " !" << endl;
		safe = true;
		break;
	case 5: item->gold = 0;
		item->name = "Cursed Stone";
		cout << "Oh no! You found a " << item->name << " !" << endl;
		safe = false;
		break;
	}

	*inventory = *item;
}

void sellItems(Item* inventory, int counter, int& playerGold) {
	int total = 0;
	for (int i = 0; i <= counter; i++) {
		playerGold += (inventory + i)->gold;
		cout << "sold " << (inventory + i)->name << " for " << (inventory + i)->gold << endl;
		total += (inventory + i)->gold;
	}

	cout << "total gold earned" << total << endl;
}

void enterDungeon(int& playerGold, Item* inventory) {
	bool eager = true;
	int counter = 0;
	bool safe = true;
	string answer;
	system("cls");
	cout << "PlayerGold:" << playerGold << endl;
	if (playerGold < 25) {
		cout << "not enough money" << endl;
	}

	else {

		cout << "Press any Key to enter the dungeon" << endl;
		system("pause");
		system("cls");

		cout << "you have entered the dungeon" << endl;
		playerGold -= 25;
		while (eager && safe) {
			makeItem(&inventory[counter], safe);
			if (safe && eager) {
				cout << "venture deeper? y/n :";
				cin >> answer;
				if (answer == "n" || answer == "N")
				{
					eager = false;

				}
				else if (answer == "y" || answer == "Y")
				{
					counter++;
				}
				system("cls");
			}
		}
		if (safe) {
			sellItems(inventory, counter, playerGold);
			cout << "PlayerGold:" << playerGold << endl;
		}
		else { cout << "Get  KURSED" << endl; 
		system("pause");
		}
	}
}


int main() {
	int gold = 50;
	Item* inventory = new Item[20];

	cout << "PlayerGold:" << gold << endl;
	while (gold<500 && gold>24)
		enterDungeon(gold, inventory);

	
	system("cls");
	if (gold >= 500) {
		cout << "you are a success!" << endl;
	}
	else if (gold <= 0) cout << "you failed better luck next time";
	

	system("pause");
	return 0;
}
