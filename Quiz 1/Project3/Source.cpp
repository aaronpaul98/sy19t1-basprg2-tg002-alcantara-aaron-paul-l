#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;


int whaleShop(int gold, int dif) {
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int suggest = 7;
	int option;

	for (int i = 0; i < 6; i++) {
		for (int f = 0; f < 6; f++) {
			int temporary = 0;
			if (packages[i] < packages[f]) {
				temporary = packages[i];
				packages[i] = packages[f];
				packages[f] = temporary;
			}
		}
	}

	for (int i = 0; i < 6; i++) {
		if (packages[i] + dif > 0) {
			suggest = i;
			break;
		}
	}

	system("cls");
	cout << "It seems you are short in gold."<< endl;
	system("pause");
	system("cls");

	cout << "--------------------------------------------" << endl;
	cout << "WHALE SHOP!" << endl;
	cout << "--------------------------------------------" << endl;
	cout << "Gold: " << gold << endl;
	cout << "--------------------------------------------" << endl;
	cout << "1." << packages[0] << endl;
	cout << "2." << packages[1] << endl;
	cout << "3." << packages[2] << endl;
	cout << "4." << packages[3] << endl;
	cout << "5." << packages[4] << endl;
	cout << "6." << packages[5] << endl;
	cout << "7." << packages[6] << endl;
	cout << "8.Exit" << endl;
	cout << "--------------------------------------------" << endl;
	
	switch (suggest) {
		case 0:
			cout << "We suggest you buying package 1 for " << packages[0] << " gold"<< endl;
			break;
		case 1:
			cout << "We suggest you buying package 2 for " << packages[1] << " gold" << endl;
			break;
		case 2:
			cout << "We suggest you buying package 3 for " << packages[2] << " gold" << endl;
			break;
		case 3:
			cout << "We suggest you buying package 4 for " << packages[3] << " gold" << endl;
			break;
		case 4:
			cout << "We suggest you buying package 5 for " << packages[4] << " gold" << endl;
			break;
		case 5:
			cout << "We suggest you buying package 6 for " << packages[5] << " gold" << endl;
			break;
		case 6:
			cout << "We suggest you buying package 7 for " << packages[6] << " gold" << endl;
			break;
		case 7:
			cout << "There is no package for this kind of riches" << endl;
			cout << "Can't afford it " << endl;
			return 0;
	}
	
	cout << "Buy suggested package? '1' for yes '0' for no" << endl;
	cin >> option;
	if (option == 1)
		return gold += packages[suggest];
	else
	{
		{
			cout << "Can't afford it " << endl;
			return 0;
		}
	}
}



void shop() {
	int option = 0;
	int gold = 250;
	bool happy = true;

	while (happy) {
		cout << "--------------------------------------------" << endl;
		cout << "WELCOME TO STEVEN'S SWORDS AND POINTY THINGS!" << endl;
		cout << "--------------------------------------------" << endl;
		cout << "Gold: " << gold << endl;
		cout << "--------------------------------------------" << endl;
		cout << "1.Wooden Sword: 50 gold" << endl;
		cout << "2.Long Sword: 100 gold" << endl;
		cout << "3.Broad Sword: 200 gold" << endl;
		cout << "4.Best Friend Sword: 2000 gold" << endl;
		cout << "5.WEEB Sword: 30000 gold" << endl;
		cout << "6.BIG F**'n Sword: 250000 gold" << endl;
		cout << "7.Espada ni Badjula: 9999999 gold" << endl;
		cout << "8.Exit" << endl;
		cout << "--------------------------------------------" << endl;
		cout << "What would you like to purchase?:";
		cin >> option;

		switch (option) {
		case 1: 
			if (gold>=50) {
				gold -=  50;
				cout << "Succsessfully purchased Wooden Sword for 50 gold" << endl;
			}
			else {
				gold = whaleShop(gold, gold - 50);
				gold -= 50;
			}
			break;
		case 2: 
			if (gold >= 100) {
				gold -= 100;
				cout << "Succsessfully purchased Long Sword for 100 gold" << endl;
			}
			else {
				gold = whaleShop(gold, gold - 100);
				gold -= 100;
			}
			break;
		case 3: 
			if (gold >= 200) {
				gold -= 200;
				cout << "Succsessfully purchased Broad Sword for 200 gold" << endl;
			}
			else {
				whaleShop(gold, gold - 200);
				gold -= 200;
			}
			break;
		case 4: 
			if (gold >= 2000) {
				gold -= 2000;
				cout << "Succsessfully purchased Best Friend Sword for 2000 gold" << endl;
			}
			else {
				whaleShop(gold, gold - 2000);
				gold -= 2000;
			}
			break;
		case 5: 
			if (gold >= 30000) {
				gold -= 30000;
				cout << "Succsessfully purchased WEEB Sword for 30000 gold" << endl;
			}
			else {
				whaleShop(gold, gold - 30000);
				gold -= 30000;
			}
			break;
		case 6: 
			if (gold >= 250000) {
				gold -= 250000;
			}
			else {
				whaleShop(gold, gold - 250000);
				gold -= 250000;
			}
			break;
		case 7: 
			if (gold >= 9999999) {
				gold -= 9999999;
				cout << "Succsessfully purchased Espada ni Badjula for 9999999 gold" << endl;
			}
			else {
				whaleShop(gold, gold - 9999999);
				gold -= 9999999;
			}
			break;
		case 8: return;

		default: cout << "Invalid choice try again" << endl;
		}

		system("pause");
		system("cls");
	}


}

int main() {
	
	shop();
	system("pause");
	return 0;
}