#include <iostream>
#include <string>
#include <time.h>

using namespace std;


struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};




//displays current list
void displayList(Node* head) {
	Node* temp;
	temp = head;
	do
	{
		cout << temp->name << endl;
		temp = temp->next;
	} while (temp != head);
}

//pass the cloak
void passCloak(Node*& knight,Node*& cloakHolder) {
	cloakHolder = knight;
}

//chooses a random knight
void randomKnight( Node*& cloakHolder,int size) {
	srand(time(NULL));
	int random = rand() % size+1;
	
	cout << cloakHolder->name << " has rolled " << random << endl;
	for (int i = 0; i < random; i++) {
		passCloak(cloakHolder->next, cloakHolder);
	}	
}
// sets the starting cloak holder
void randomStart(Node*& head, Node*& cloakHolder, int size) {
	srand(time(NULL));
	int random = rand() % size + 1;
	for (int i = 0; i < random; i++) {
		if(i==0)
			passCloak(head, cloakHolder);
		else passCloak(cloakHolder->next, cloakHolder);
	}

}
// Removes the knight from the circle if chosen
void removeNode(Node*& cloakHolder, Node*& head) {
	Node* temp = NULL;
			
		if (head->next == cloakHolder) {
			temp = cloakHolder;
			head->next = cloakHolder->next;
		}
		else {
			removeNode(cloakHolder, head->next);
		}

	delete temp;
}

// Adds Node
void addNode(Node*& head, Node*& tail) {

	Node* temp = new Node;
	string name;

	cout << "what is your name? :";
	cin >> name;

	temp->name = name;

	if (head == NULL) { // First node
		head = temp;
		tail = temp;
	}
	else {	//every succeeding node

		tail->next = temp;
		tail = tail->next;	
		tail->next = head;
	}
}


int main() {
	
	Node* head = NULL;// first knight
	Node* tail = NULL;// first knight
	Node* cloakHolder = NULL; //cloak holder
	
	int size = 0;
	int roundNo = 1;

	//select number of soldiers
	cout << "How many soldiers are there?: ";
	cin >> size;
	//create soldiers
	for (int i = 0; i < size; i++) {
		addNode(head, tail);
	}
	system("cls");

	//select start 

	randomStart(head, cloakHolder, size);
	cout << " The Lord Commander has chosen " << cloakHolder->name << " as the first holder of the cloak" << endl;

	// while(size>1) Run Game
	while (size > 1) {
		Node*  toBeDeleted = NULL;
		cout << "====================================" << endl;
		cout << "ROUND " << roundNo << endl;
		cout << "===================================="<<endl;

		displayList(cloakHolder);
		
		//roll for random

		cout << endl;
		randomKnight(cloakHolder, size);
		toBeDeleted = cloakHolder;
		cout << cloakHolder->name << " has been eliminated " << endl;

		//pass cloak

		passCloak(cloakHolder->next, cloakHolder);
		cout << endl;
		system("pause");

		//delete prevoius cloak holder

		removeNode(toBeDeleted, cloakHolder);
		size--;
		roundNo++;
		system("cls");
	}
	cout << "====================================" << endl;
	cout << "FINAL RESULT" << endl;
	cout << "====================================" << endl;
	cout << cloakHolder->name << " will go to seek for reinforcements." << endl;
	system("pause");
	return 0;
}