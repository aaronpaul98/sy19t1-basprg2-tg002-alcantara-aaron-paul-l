#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

enum CardType { Slave, Civilian, Emperor };
enum HandType { SlaveHand, EmperorHand};
enum Winner {Player,AI,Draw};


struct Card {
	CardType Card;
};

//Generates a hand for the player
void drawHand(vector<Card>& hand,HandType handType) {
	srand(time(NULL));
	Card temp = {Civilian};

	for (int i = 0; i < 5; i++) {
		hand.push_back(temp);
	}

	if (handType == SlaveHand) {
		hand[0].Card = Slave;
		hand[1].Card = Civilian;
		hand[2].Card = Civilian;
		hand[3].Card = Civilian;
		hand[4].Card = Civilian;
	}

	else if (handType == EmperorHand) {
		hand[0].Card = Emperor;
		hand[1].Card = Civilian;
		hand[2].Card = Civilian;
		hand[3].Card = Civilian;
		hand[4].Card = Civilian;
	}
}

//displays player hand
void displayHand(vector<Card>& hand) {

	for (int i = 0; i < hand.size(); i++) {
		switch (hand[i].Card) {
			case Slave : cout << i+1 << " : "<<"[Slave]" << endl;
				break;
			case Civilian: cout << i+1 << " : " << "[Civilian]" << endl;
				break;
			case Emperor: cout << i+1 << " : " << "[Emperor]" << endl;
				break;
		}
	}

}

//Player plays a Card
Card playCard(vector<Card>& hand,int cardPlayed) {
	
	Card temp = hand[cardPlayed];
	hand.erase(hand.begin() + cardPlayed);

	return temp;
}


//Checks winner
Winner checkWinner(Card playerCard, Card aiCard) {
	if (playerCard.Card == Emperor && aiCard.Card == Civilian)
		return Player;
	else if (playerCard.Card == Emperor && aiCard.Card == Slave)
		return AI;
	else if (playerCard.Card == Slave && aiCard.Card == Civilian)
		return AI;
	else if (aiCard.Card == Emperor && playerCard.Card == Civilian)
		return AI;
	else if (aiCard.Card == Emperor && playerCard.Card == Slave)
		return Player;
	else if (aiCard.Card == Slave && playerCard.Card == Civilian)
		return Player;
	else return Draw;
}

//Input Bet
void getBet(int mm,int& bet) {
	cout << "How much will you bet?:";
	cin >> bet;
}

void emptyHands(vector<Card>& handPlayer, vector<Card>& handAI) {
	for (int i = handPlayer.size(); i > 0;i--) {
		handPlayer.pop_back();
	}
	for (int i = handAI.size(); i > 0; i--) {
		handAI.pop_back();
	}
}

void pickCard(int& choice, vector<Card> handPlayer) {
	cout << "Which card are you going to play?: ";
	cin >> choice;

	if (choice > handPlayer.size()) {
		cout << "invalid card choice" << endl;
		pickCard(choice, handPlayer);
	}
}

//Plays a round
void playRound(int& round, int& money, int& mm,  int& bet , vector<Card>& handPlayer, vector<Card>& handAI) {

	displayHand(handPlayer);
	
	int choice;
	pickCard(choice,handPlayer);

	

	Card playerCard;
	Card aiCard;

	playerCard = playCard(handPlayer, choice - 1);
	int random = rand() % handAI.size();
	aiCard = playCard(handAI, random);

	switch (playerCard.Card) {
	case Slave: cout <<"You played [Slave Card]" << endl;
		break;
	case Civilian: cout << "You played [Civilian Card]" << endl;
		break;
	case Emperor: cout << "You played [Emperor Card]" << endl;
		break;
	}

	switch (aiCard.Card) {
	case Slave: cout << "Opponent played [Slave Card]" << endl;
		break;
	case Civilian: cout << "Opponent [Civilian Card]" << endl;
		break;
	case Emperor: cout << "Opponent [Emperor Card]" << endl;
		break;
	}
	Winner dinner = checkWinner(playerCard, aiCard);


	if (dinner == Player) {
		if ((round >= 1 && round <= 3) || (round >= 7 && round <= 9))
		{
			money += bet * 100000;
			emptyHands(handPlayer, handAI);

		}
		else if ((round >= 4 && round <= 6) || (round >= 10 && round <= 12))
		{
			money += bet * 500000;
			emptyHands(handPlayer, handAI);
		}
		cout << "You win!" << endl;
		system("pause");
		system("cls");
	}
	else if (dinner == AI) {
		cout << "You lose!" << endl;
		mm -= bet;
		emptyHands(handPlayer, handAI);
		system("pause");
		system("cls");
	}
	else if (dinner == Draw) {
		cout << "Its a draw!" << endl;
		system("pause");
		system("cls");
		playRound(round, money, mm, bet,handPlayer, handAI);
	}
}

void checkEnding(int money, int millimeters) {
	system("clear");
	if (millimeters <= 0) {
		cout << " The device has pierced your ear drum" << endl;
		cout << "              BAD END " << endl;
	}
	else if (millimeters > 0 && money < 20000000) {
		cout << " You did not reach your goal. But at  least you are whole." << endl;
		cout << "              MEH END " << endl;

	}
	else if (millimeters > 0 && money >= 20000000) {
		cout << "		    BITCH YOU RICH!" << endl;
		cout << "              MEH END " << endl;
	}
}

int main() {
	int rounds = 12;
	int currRound = 1;
	int millimeters = 30;
	int money = 0;
	
	vector<Card> handPlayer(0);
	vector<Card> handAI(0);

	while (currRound != rounds && millimeters != 0) {
		int bet = 0;
		string side = " ";

		if ((currRound >= 1 && currRound <= 3) || (currRound >= 7 && currRound <= 9))
		{
			drawHand(handPlayer, EmperorHand);
			drawHand(handAI, SlaveHand);
			side = "Emperor";
		}
		else if ((currRound >= 4 && currRound <= 6) || (currRound >= 10 && currRound <= 12))
		{
			drawHand(handPlayer, SlaveHand);
			drawHand(handAI, EmperorHand);
			side = "Slave";
		}

		cout << "Round:" << currRound << endl;
		cout << "Money:" << money << endl;
		cout << "Millimeters Left:" << millimeters << endl;
		cout << "You are playing as the " << side << endl;
		getBet(millimeters,bet);
		system("cls");

		playRound(currRound, money, millimeters, bet, handPlayer, handAI);
		currRound++;
	}




	checkEnding(money, millimeters);

	system("pause");
	return 0;
}